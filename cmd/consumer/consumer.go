package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"sync"

	"github.com/IBM/sarama"
	"github.com/SysIntruder/go-kafka-notify/pkg/models"
	"github.com/labstack/echo/v4"
)

const (
	ConsumerGroup      = "notifications-group"
	ConsumerTopic      = "notifications"
	ConsumerPort       = ":8081"
	KafkaServerAddress = "localhost:9092"
)

type (
	UserNotifications map[string][]models.Notification

	NotificationStore struct {
		data UserNotifications
		mu   sync.Mutex
	}

	Consumer struct {
		store *NotificationStore
	}
)

func (ns *NotificationStore) Add(userID string, notification models.Notification) {
	ns.mu.Lock()
	defer ns.mu.Unlock()
	ns.data[userID] = append(ns.data[userID], notification)
}

func (ns *NotificationStore) Get(userID string) []models.Notification {
	ns.mu.Lock()
	defer ns.mu.Unlock()
	return ns.data[userID]
}

func (*Consumer) Setup(sarama.ConsumerGroupSession) error   { return nil }
func (*Consumer) Cleanup(sarama.ConsumerGroupSession) error { return nil }

func (c *Consumer) ConsumeClaim(
	sess sarama.ConsumerGroupSession,
	claim sarama.ConsumerGroupClaim,
) error {
	for msg := range claim.Messages() {
		userID := string(msg.Key)
		var notification models.Notification
		err := json.Unmarshal(msg.Value, &notification)
		if err != nil {
			return err
		}
		c.store.Add(userID, notification)
		sess.MarkMessage(msg, "consumed")
	}
	return nil
}

func initConsumerGroup() (sarama.ConsumerGroup, error) {
	cfg := sarama.NewConfig()

	consumerGroup, err := sarama.NewConsumerGroup([]string{KafkaServerAddress}, ConsumerGroup, cfg)
	if err != nil {
		return nil, err
	}
	return consumerGroup, nil
}

func setupConsumerGroup(ctx context.Context, store *NotificationStore) {
	consumerGroup, err := initConsumerGroup()
	if err != nil {
		log.Println(err)
	}
	defer consumerGroup.Close()
	consumer := &Consumer{
		store: store,
	}
	for {
		err := consumerGroup.Consume(ctx, []string{ConsumerTopic}, consumer)
		if err != nil {
			log.Println(err)
		}
		if ctx.Err() != nil {
			return
		}
	}
}

func handleNotifications(c echo.Context, store *NotificationStore) error {
	userID := c.Param("userID")
	notes := store.Get(userID)
	if len(notes) == 0 {
		return c.JSON(http.StatusOK, "No Notification")
	}

	return c.JSON(http.StatusOK, notes)
}

func main() {
	store := &NotificationStore{
		data: make(UserNotifications),
	}

	ctx, cancel := context.WithCancel(context.Background())
	go setupConsumerGroup(ctx, store)
	defer cancel()

	e := echo.New()
	e.GET("/:userID", func(c echo.Context) error {
		res := handleNotifications(c, store)
		return res
	})

	e.Logger.Fatal(
		e.Start(ConsumerPort),
	)
}
