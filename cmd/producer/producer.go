package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	"github.com/IBM/sarama"
	"github.com/SysIntruder/go-kafka-notify/pkg/models"
	"github.com/labstack/echo/v4"
)

const (
	ProducerPort       = ":8080"
	KafkaServerAddress = "localhost:9092"
	KafkaTopic         = "notifications"
)

func userByID(id int, users []models.User) (models.User, error) {
	for _, user := range users {
		if user.ID == id {
			return user, nil
		}
	}
	return models.User{}, errors.New("user not found in producer")
}

func sendMessage(
	producer sarama.SyncProducer,
	users []models.User,
	c echo.Context,
	fromID, toID int,
) error {
	from, err := userByID(fromID, users)
	if err != nil {
		return err
	}
	to, err := userByID(toID, users)
	if err != nil {
		return nil
	}
	message := c.FormValue("message")

	notification := models.Notification{
		From:    from,
		To:      to,
		Message: message,
	}
	jsonNotification, err := json.Marshal(notification)
	if err != nil {
		return err
	}

	msg := &sarama.ProducerMessage{
		Topic: KafkaTopic,
		Key:   sarama.StringEncoder(strconv.Itoa(to.ID)),
		Value: sarama.StringEncoder(jsonNotification),
	}

	_, _, err = producer.SendMessage(msg)
	return err
}

func handleSendMessage(producer sarama.SyncProducer, users []models.User) func(echo.Context) error {
	return func(c echo.Context) error {
		rawFromID := c.FormValue("fromID")
		fromID, err := strconv.Atoi(rawFromID)
		if err != nil {
			return err
		}

		rawToID := c.FormValue("toID")
		toID, err := strconv.Atoi(rawToID)
		if err != nil {
			return err
		}

		err = sendMessage(producer, users, c, fromID, toID)
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, "Notification sent!")
	}
}

func setupProducer() (sarama.SyncProducer, error) {
	cfg := sarama.NewConfig()
	cfg.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer([]string{KafkaServerAddress}, cfg)
	if err != nil {
		return nil, err
	}
	return producer, nil
}

func main() {
	users := []models.User{
		{ID: 1, Name: "Kiana"},
		{ID: 2, Name: "Mei"},
		{ID: 3, Name: "Bronya"},
		{ID: 4, Name: "Elysia"},
	}

	producer, err := setupProducer()
	if err != nil {
		log.Fatalf("failed to initialize producer: %v", err)
	}
	defer producer.Close()

	e := echo.New()
	e.POST("/send", handleSendMessage(producer, users))

	e.Logger.Fatal(
		e.Start(ProducerPort),
	)
}
